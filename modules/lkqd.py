import requests
import json
from operator import itemgetter

class LKQD:

	def post_req(dates):
		print("[+]Requesting LKQD Data")
		d_range = ['today', 'yesterday', 'sdlw']
		data = []

		creds = {
			"key": "Xan9879bYCH6336WkLJ6wvc4X2xiPikO",
			"secret": "7JyOpXN9wV9BVNNyHjTRsqcDo2eOsykgY29LwLSzvgM"
		}

		url = "https://{0}:{1}@api.lkqd.com/reports".format(creds['key'], creds['secret'])


		for i in d_range:

			opts = {
				'timeDimension': 'HOURLY',
				'startHour': '0',
				'endHour': '23',
				'reportType': [],
				'reportFormat': 'JSON',
				'timezone': 'America/New_York',
				'metrics': ["OPPORTUNITIES", "IMPRESSIONS", "REVENUE"],
				'startDate': "{}".format(dates[i]),
				'endDate': "{}".format(dates[i])
			}


			r = requests.Session()
			try:
				resp = r.post(url, json=opts)
			except Exception as e:
				print(e)
			if(resp.status_code == 200):
				lkqd_dict = json.loads(resp.text)
				lkqd_arr = sorted(lkqd_dict['data']['entries'], key=itemgetter('timeDimension'))
				data.append(lkqd_arr)
		print(data)
		return data