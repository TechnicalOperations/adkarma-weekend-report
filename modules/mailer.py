#!/usr/bin/python

import imaplib
import smtplib
import email
import os
import sys
import re
import csv
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
import email.encoders as Encoders
import time
from email.mime.text import MIMEText


def send_email():
	print("[+]Sending updated report")

	SUBJECT = "Weekend Report"
	FROM = "tops.report@rhythmone.com"
	TO = "mgillespie@rhythmone.com"
	#CC = 'tops@rhythmone.com'

	msg = MIMEMultipart()
	msg['Subject'] = SUBJECT
	msg['From'] = FROM
	msg['To'] = TO
	#msg['Cc'] = CC


	part = MIMEBase('application', "octet-stream")
	part.set_payload(open("../excel/test.xlsx", "rb").read())
	Encoders.encode_base64(part)

	part.add_header('Content-Disposition', 'attachment; filename="{}"'.format("../excel/test.xlsx"))
	msg.attach(part)

	html = """\
		<html>
			<head></head>
			<body>
				<p>Hey, Team!</p>
				<p>Overall, we are down TODAY from YESTERDAY and SDLW</p>
				<p>SPRINGSERVE is DOWN across the board.<br>
				LKQD is DOWN across the board.</p>
			</body>
		</html>
	"""
	html_text = MIMEText(html, 'html')
	msg.attach(html_text)

	smtp = smtplib.SMTP('outlook.office365.com', 587)
	smtp.ehlo()
	smtp.starttls()
	smtp.ehlo
	smtp.login('tops.report@rhythmone.com', '3QCZbUmqYAF6mJC8FEPd')
	smtp.sendmail(FROM, [TO], str(msg))


if __name__ == '__main__':
	send_email()