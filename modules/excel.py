import xlsxwriter


class Excel:
	def build_doc(ss_data,lkqd_data,dates):
		# Create Excel Workbook
		print("\t[-]Building Excel doc")
		try:
			wb = xlsxwriter.Workbook('excel/test.xlsx')
			summary_ws = wb.add_worksheet('Summary')
			comp_ws = wb.add_worksheet('Comparison')
			today_ws = wb.add_worksheet('Today')
			yest_ws = wb.add_worksheet('Yesterday')
			sdlw_ws = wb.add_worksheet('SDLW')
			pmp_ws = wb.add_worksheet('PMP Activity')
		except Exception as e:
			print("[!]Error")
			print(e)

		merge_format_ss = wb.add_format({
			'bold': 1,
			'border': 1,
			'align': 'center',
			'valign': 'vcenter',
			'fg_color': '#FFF2CC'})
		merge_format_lkqd = wb.add_format({
			'bold': 1,
			'border': 1,
			'align': 'center',
			'valign': 'vcenter',
			'fg_color': '#FFCCCC'})
		format_header = wb.add_format({
			'bold': 1,
			'border': 1,
			'align': 'center',
			'valign': 'vcenter',
			'fg_color': '#E7E6E6'})
		fillrate_header = wb.add_format({
			'bold': 1,
			'border': 1,
			'align': 'center',
			'valign': 'vcenter',
			'fg_color': '#E7E6E6',
			'num_format': '0.00"%"'})

		sum_header = wb.add_format({
			'bold': 1,
			'border': 1,
			'align': 'center',
			'valign': 'vcenter'})

		comp_header = wb.add_format({
			'border': 1,
			'align': 'center',
			'fg_color': '#E7E6E6',
			'valign': 'vcenter'})

		comptotal_header = wb.add_format({
			'border': 1,
			'align': 'center',
			'valign': 'vcenter',
			'fg_color': '#E7E6E6',
			'num_format': '0.00"%"'})

		def today_sheet():
			today_ws.set_column('A:M', 10)

			today_ws.merge_range('C1:F1', 'SpringServe', merge_format_ss)
			today_ws.merge_range('G1:J1', 'LKQD', merge_format_lkqd)
			today_ws.write('A2', "Date", format_header)
			today_ws.write('B2', "Hour",format_header)
			today_ws.write('C2', "Inventory",format_header)
			today_ws.write('D2', "Imps",format_header)
			today_ws.write('E2', "Rev",format_header)
			today_ws.write('F2', "Fill Rate",format_header)
			today_ws.write('G2', "Inventory",format_header)
			today_ws.write('H2', "Imps",format_header)
			today_ws.write('I2', "Rev",format_header)
			today_ws.write('J2', "Fill Rate",format_header)

			print(lkqd_data)
			date = ""
			hour = ""
			row = 3
			for i in range(0,len(ss_data['today'])):
				x = ss_data['today'].at[i,'date']
				date,hour = x.split(" ")
				hour = hour.split(":")[0]
				today_ws.write('A{}'.format(row), "{}".format(date))
				today_ws.write('B{}'.format(row), "{}".format(hour))

				opp = int(ss_data['today'].at[i, 'opportunities'])
				today_ws.write('C{}'.format(row), opp)

				imp = int(ss_data['today'].at[i, 'total_impressions'])
				today_ws.write('D{}'.format(row), imp)

				rev = float(ss_data['today'].at[i, 'revenue'])
				today_ws.write('E{}'.format(row), rev)

				fill_rate = "(D%s/C%s)*100" % (row,row)
				today_ws.write_array_formula('F{}'.format(row), fill_rate, fillrate_header)

				row += 1

			row = 3
			for i in lkqd_data[0]:
				opp = int(i['adOpportunities'])
				#today_ws.write('G{}'.format(row), opp)
				today_ws.write_array_formula('G{}'.format(row), '=IF(C%s <> "", %s, ""' % (row,opp))

				imp = int(i['adImpressions'])
				#today_ws.write('H{}'.format(row), imp)
				today_ws.write_array_formula('H{}'.format(row), '=IF(D%s <> "", %s, ""' % (row,imp))

				rev = float(i['revenue'])
				#today_ws.write('I{}'.format(row), rev)
				today_ws.write_array_formula('I{}'.format(row), '=IF(E%s <> "", %s, ""' % (row,rev))

				#fill_rate = "=(H%s/G%s)*100" % (row,row)
				fill_rate = '=IF(F%s <> "", (H%s/G%s)*100, ""' % (row,row,row)
				today_ws.write_array_formula('J{}'.format(row), fill_rate, fillrate_header)

				row += 1




		def yest_sheet():
			yest_ws.set_column('A:M', 10)

			yest_ws.merge_range('C1:F1', 'SpringServe', merge_format_ss)
			yest_ws.merge_range('G1:J1', 'LKQD', merge_format_lkqd)
			yest_ws.write('A2', "Date", format_header)
			yest_ws.write('B2', "Hour", format_header)
			yest_ws.write('C2', "Inventory", format_header)
			yest_ws.write('D2', "Imps", format_header)
			yest_ws.write('E2', "Rev", format_header)
			yest_ws.write('F2', "Fill Rate", format_header)
			yest_ws.write('G2', "Inventory", format_header)
			yest_ws.write('H2', "Imps", format_header)
			yest_ws.write('I2', "Rev", format_header)
			yest_ws.write('J2', "Fill Rate", format_header)

			date = ""
			hour = ""
			row = 3
			for i in range(0,len(ss_data['yesterday'])):
				x = ss_data['yesterday'].at[i,'date']
				date,hour = x.split(" ")
				hour = hour.split(":")[0]
				yest_ws.write('A{}'.format(row), "{}".format(date))
				yest_ws.write('B{}'.format(row), "{}".format(hour))

				opp = int(ss_data['yesterday'].at[i, 'opportunities'])
				yest_ws.write('C{}'.format(row), opp)

				imp = int(ss_data['yesterday'].at[i, 'total_impressions'])
				yest_ws.write('D{}'.format(row), imp)

				rev = float(ss_data['yesterday'].at[i, 'revenue'])
				yest_ws.write('E{}'.format(row), rev)

				fill_rate = "=(D%s/C%s)*100" % (row,row)
				yest_ws.write_array_formula('F{}'.format(row), fill_rate, fillrate_header)

				row += 1

			row = 3
			for i in lkqd_data[1]:
				opp = int(i['adOpportunities'])
				yest_ws.write('G{}'.format(row), opp)

				imp = int(i['adImpressions'])
				yest_ws.write('H{}'.format(row), imp)

				rev = float(i['revenue'])
				yest_ws.write('I{}'.format(row), rev)

				fill_rate = "=(H%s/G%s)*100" % (row, row)
				yest_ws.write_array_formula('J{}'.format(row), fill_rate, fillrate_header)

				row += 1

		def sdlw_sheet():
			sdlw_ws.set_column('A:M', 10)

			sdlw_ws.merge_range('C1:F1', 'SpringServe', merge_format_ss)
			sdlw_ws.merge_range('G1:J1', 'LKQD', merge_format_lkqd)
			sdlw_ws.write('A2', "Date", format_header)
			sdlw_ws.write('B2', "Hour", format_header)
			sdlw_ws.write('C2', "Inventory", format_header)
			sdlw_ws.write('D2', "Imps", format_header)
			sdlw_ws.write('E2', "Rev", format_header)
			sdlw_ws.write('F2', "Fill Rate", format_header)
			sdlw_ws.write('G2', "Inventory", format_header)
			sdlw_ws.write('H2', "Imps", format_header)
			sdlw_ws.write('I2', "Rev", format_header)
			sdlw_ws.write('J2', "Fill Rate", format_header)

			date = ""
			hour = ""
			row = 3
			for i in range(0,len(ss_data['sdlw'])):
				x = ss_data['sdlw'].at[i,'date']
				date,hour = x.split(" ")
				hour = hour.split(":")[0]
				sdlw_ws.write('A{}'.format(row), "{}".format(date))
				sdlw_ws.write('B{}'.format(row), "{}".format(hour))

				opp = int(ss_data['sdlw'].at[i, 'opportunities'])
				sdlw_ws.write('C{}'.format(row), opp)

				imp = int(ss_data['sdlw'].at[i, 'total_impressions'])
				sdlw_ws.write('D{}'.format(row), imp)

				rev = float(ss_data['sdlw'].at[i, 'revenue'])
				sdlw_ws.write('E{}'.format(row), rev)

				fill_rate = "=(D%s/C%s)*100" % (row,row)
				sdlw_ws.write_array_formula('F{}'.format(row), fill_rate, fillrate_header)

				row += 1

			row = 3
			for i in lkqd_data[2]:
				opp = int(i['adOpportunities'])
				sdlw_ws.write('G{}'.format(row), opp)

				imp = int(i['adImpressions'])
				sdlw_ws.write('H{}'.format(row), imp)

				rev = float(i['revenue'])
				sdlw_ws.write('I{}'.format(row), rev)

				fill_rate = "=(H%s/G%s)*100" % (row, row)
				sdlw_ws.write_array_formula('J{}'.format(row), fill_rate, fillrate_header)

				row += 1


		def comp_sheet():
			comp_ws.set_column('A:M', 15)

			comp_ws.merge_range('A1:M1', 'SpringServe', merge_format_ss)
			comp_ws.merge_range('A30:M30', 'LKQD', merge_format_lkqd)
			comp_ws.merge_range('B2:E2', 'Today (%s)' % dates['today'], sum_header)
			comp_ws.merge_range('B31:E31', 'Today (%s)' % dates['today'], sum_header)
			comp_ws.merge_range('F2:I2', 'Yesterday (%s)' % dates['yesterday'], sum_header)
			comp_ws.merge_range('F31:I31', 'Yesterday (%s)' % dates['yesterday'], sum_header)
			comp_ws.merge_range('J2:M2', 'SDLW (%s)' % dates['sdlw'], sum_header)
			comp_ws.merge_range('J31:M31', 'SDLW (%s)' % dates['sdlw'], sum_header)

			comp_ws.write('A3', 'Hour', sum_header)
			comp_ws.write('A32', 'Hour', sum_header)

			comp_ws.write('A28', 'TOTAL', sum_header)
			comp_ws.write('A57', 'TOTAL', sum_header)

			n = 0
			p = 4
			for i in range(0,24):
				comp_ws.write('A{}'.format(p), str(n))
				n += 1
				p+=1

			n = 0
			p = 33
			for i in range(0,24):
				comp_ws.write('A{}'.format(p), str(n))
				n += 1
				p+=1

			today_stats = ['B3','C3','D3','E3',
						   'B32','C32','D32','E32']
			yest_stats = ['F3','G3','H3','I3',
						  'F32','G32','H32','I32']
			sdlw_stats = ['J3','K3','L3','M3',
						  'J32','K32','L32','M32']
			stats = ['Inventory','Ad Impressions','Rev','Fill Rate']

			n = 0
			for i in today_stats:
				comp_ws.write(i, stats[n], comp_header)
				n += 1
				if n > 3:
					n = 0
			n = 0
			for i in yest_stats:
				comp_ws.write(i, stats[n], comp_header)
				n += 1
				if n > 3:
					n = 0
			n = 0
			for i in sdlw_stats:
				comp_ws.write(i, stats[n], comp_header)
				n += 1
				if n > 3:
					n = 0

			n = 3
			for i in range(4,28):
				comp_ws.write_array_formula('B{}'.format(i), '=IF(Today!C%s="","",Today!C%s)' % (n, n), comp_header)
				comp_ws.write_array_formula('C{}'.format(i), '=IF(Today!D%s="","",Today!D%s)' % (n, n), comp_header)
				comp_ws.write_array_formula('D{}'.format(i), '=IF(Today!E%s="","",Today!E%s)' % (n, n), comp_header)
				comp_ws.write_array_formula('E{}'.format(i), '=IF(Today!F%s="","",Today!F%s)' % (n, n), comptotal_header)

				comp_ws.write_array_formula('F{}'.format(i), '=IF($B%s="","",Yesterday!C%s)' % (i, n), comp_header)
				comp_ws.write_array_formula('G{}'.format(i), '=IF($B%s="","",Yesterday!D%s)' % (i, n), comp_header)
				comp_ws.write_array_formula('H{}'.format(i), '=IF($B%s="","",Yesterday!E%s)' % (i, n), comp_header)
				comp_ws.write_array_formula('I{}'.format(i), '=IF($B%s="","",Yesterday!F%s)' % (i, n), comptotal_header)

				comp_ws.write_array_formula('J{}'.format(i), '=IF($B%s="","",SDLW!C%s)' % (n+1, n), comp_header)
				comp_ws.write_array_formula('K{}'.format(i), '=IF($B%s="","",SDLW!D%s)' % (n+1, n), comp_header)
				comp_ws.write_array_formula('L{}'.format(i), '=IF($B%s="","",SDLW!E%s)' % (n+1, n), comp_header)
				comp_ws.write_array_formula('M{}'.format(i), '=IF($B%s="","",SDLW!F%s)' % (n+1, n), comptotal_header)
				n += 1


			comp_ws.write_array_formula('B28'.format(i), '=IF(B4="","",SUM(B4:B27))', comp_header)
			comp_ws.write_array_formula('C28'.format(i), '=IF(C4="","",SUM(C4:C27))', comp_header)
			comp_ws.write_array_formula('D28'.format(i), '=IF(D4="","",SUM(D4:D27))', comp_header)
			comp_ws.write_array_formula('E28'.format(i), '=IFERROR((C28/B28)*100,"")', comptotal_header)

			comp_ws.write_array_formula('F28'.format(i), '=IF(F4="","",SUM(F4:F27))', comp_header)
			comp_ws.write_array_formula('G28'.format(i), '=IF(G4="","",SUM(G4:G27))', comp_header)
			comp_ws.write_array_formula('H28'.format(i), '=IF(H4="","",SUM(H4:H27))', comp_header)
			comp_ws.write_array_formula('I28'.format(i), '=IFERROR((G28/F28)*100,"")', comptotal_header)

			comp_ws.write_array_formula('J28'.format(i), '=IF(J4="","",SUM(J4:J27))', comp_header)
			comp_ws.write_array_formula('K28'.format(i), '=IF(K4="","",SUM(K4:K27))', comp_header)
			comp_ws.write_array_formula('L28'.format(i), '=IF(L4="","",SUM(L4:L27))', comp_header)
			comp_ws.write_array_formula('M28'.format(i), '=IFERROR((K28/J28)*100,"")', comptotal_header)



			n = 3
			for i in range(33, 57):
				comp_ws.write_array_formula('B{}'.format(i), '=IF(Today!G%s="","",Today!G%s)' % (n, n), comp_header)
				comp_ws.write_array_formula('C{}'.format(i), '=IF(Today!H%s="","",Today!H%s)' % (n, n), comp_header)
				comp_ws.write_array_formula('D{}'.format(i), '=IF(Today!I%s="","",Today!I%s)' % (n, n), comp_header)
				comp_ws.write_array_formula('E{}'.format(i), '=IF(Today!J%s="","",Today!J%s)' % (n, n), comptotal_header)

				comp_ws.write_array_formula('F{}'.format(i), '=IF($B%s="","",Yesterday!G%s)' % (i, n), comp_header)
				comp_ws.write_array_formula('G{}'.format(i), '=IF($B%s="","",Yesterday!H%s)' % (i, n), comp_header)
				comp_ws.write_array_formula('H{}'.format(i), '=IF($B%s="","",Yesterday!I%s)' % (i, n), comp_header)
				comp_ws.write_array_formula('I{}'.format(i), '=IF($B%s="","",Yesterday!J%s)' % (i, n), comptotal_header)

				comp_ws.write_array_formula('J{}'.format(i), '=IF($B%s="","",SDLW!G%s)' % (n + 30, n), comp_header)
				comp_ws.write_array_formula('K{}'.format(i), '=IF($B%s="","",SDLW!H%s)' % (n + 30, n), comp_header)
				comp_ws.write_array_formula('L{}'.format(i), '=IF($B%s="","",SDLW!I%s)' % (n + 30, n), comp_header)
				comp_ws.write_array_formula('M{}'.format(i), '=IF($B%s="","",SDLW!J%s)' % (n + 30, n), comptotal_header)
				n += 1

			comp_ws.write_array_formula('B57'.format(i), '=IF(B33="","",SUM(B33:B56))', comp_header)
			comp_ws.write_array_formula('C57'.format(i), '=IF(C33="","",SUM(C33:C56))', comp_header)
			comp_ws.write_array_formula('D57'.format(i), '=IF(D33="","",SUM(D33:D56))', comp_header)
			comp_ws.write_array_formula('E57'.format(i), '=IFERROR((C57/B57)*100,"")', comptotal_header)

			comp_ws.write_array_formula('F57'.format(i), '=IF(F33="","",SUM(F33:F56))', comp_header)
			comp_ws.write_array_formula('G57'.format(i), '=IF(G33="","",SUM(G33:G56))', comp_header)
			comp_ws.write_array_formula('H57'.format(i), '=IF(H33="","",SUM(H33:H56))', comp_header)
			comp_ws.write_array_formula('I57'.format(i), '=IFERROR((G57/F57)*100,"")', comptotal_header)

			comp_ws.write_array_formula('J57'.format(i), '=IF(J33="","",SUM(J33:J56))', comp_header)
			comp_ws.write_array_formula('K57'.format(i), '=IF(K33="","",SUM(K33:K56))', comp_header)
			comp_ws.write_array_formula('L57'.format(i), '=IF(L33="","",SUM(L33:L56))', comp_header)
			comp_ws.write_array_formula('M57'.format(i), '=IFERROR((K57/J57)*100,"")', comptotal_header)


		def sum_sheet():

			summary_ws.set_column('A:M', 23)


			summary_ws.merge_range('A2:D2', 'SpringServe - Total', merge_format_ss)
			summary_ws.merge_range('F2:H2', 'SpringServe - % Change', merge_format_ss)
			summary_ws.merge_range('A9:D9', 'SpringServe & LKQD - Total', merge_format_ss)
			summary_ws.merge_range('F9:H9', 'SpringServe - % Change', merge_format_ss)
			summary_ws.merge_range('A16:D16', 'LKQD - Total', merge_format_ss)
			summary_ws.merge_range('F16:H16', 'LKQD - % Change', merge_format_ss)



			inv_header = ['A4','F4','A11','F11','A18','F18']
			adimp_header = ['A5','F5','A12','F12','A19','F19']
			adrev_header = ['A6','F6','A13','F13','A20','F20']
			fill_header = ['A7','F7','A14','F14','A21','F21']
			todayvs_header = ['F3','F10','F17']
			today_date = ['B3','B10','B17']
			yest_date = ['C3','C10','C17','G3','G10','G17']
			sdlw_date = ['D3','D10','D17','H3','H10','H17']

			for i in today_date:
				summary_ws.write(i, 'Today (%s)' % dates['today'], sum_header)
			for i in yest_date:
				summary_ws.write(i, 'Yesterday (%s)' % dates['yesterday'], sum_header)
			for i in sdlw_date:
				summary_ws.write(i, 'SDLW (%s)' % dates['sdlw'], sum_header)
			for i in inv_header:
				summary_ws.write(i, 'Inventory', sum_header)
			for i in adimp_header:
				summary_ws.write(i, 'Ad Impressions', sum_header)
			for i in adrev_header:
				summary_ws.write(i, 'Ad Revenue', sum_header)
			for i in fill_header:
				summary_ws.write(i, 'Fill Rate', sum_header)
			for i in todayvs_header:
				summary_ws.write(i, 'Today vs.', sum_header)



			# SpringServe - Total
			summary_ws.write_array_formula('B4', '=IF(Comparison!B28="","",Comparison!B28)', comp_header)
			summary_ws.write_array_formula('B5', '=IF(Comparison!C28="","",Comparison!C28)', comp_header)
			summary_ws.write_array_formula('B6', '=IF(Comparison!D28="","",Comparison!D28)', comp_header)
			summary_ws.write_array_formula('B7', '=IF(Comparison!E28="","",Comparison!E28)', comptotal_header)

			summary_ws.write_array_formula('C4', '=IF(Comparison!F28="","",Comparison!F28)', comp_header)
			summary_ws.write_array_formula('C5', '=IF(Comparison!G28="","",Comparison!G28)', comp_header)
			summary_ws.write_array_formula('C6', '=IF(Comparison!H28="","",Comparison!H28)', comp_header)
			summary_ws.write_array_formula('C7', '=IF(Comparison!I28="","",Comparison!I28)', comptotal_header)

			summary_ws.write_array_formula('D4', '=IF(Comparison!J28="","",Comparison!J28)', comp_header)
			summary_ws.write_array_formula('D5', '=IF(Comparison!K28="","",Comparison!K28)', comp_header)
			summary_ws.write_array_formula('D6', '=IF(Comparison!L28="","",Comparison!L28)', comp_header)
			summary_ws.write_array_formula('D7', '=IF(Comparison!M28="","",Comparison!M28)', comptotal_header)

			# SpringServe - % Change
			summary_ws.write_array_formula('G4', '=IFERROR((B4/C4)-1,"")', comptotal_header)
			summary_ws.write_array_formula('G5', '=IFERROR((B5/C5)-1,"")', comptotal_header)
			summary_ws.write_array_formula('G6', '=IFERROR((B6/C6)-1,"")', comptotal_header)
			summary_ws.write_array_formula('G7', '=IFERROR((B7/C7)-1,"")', comptotal_header)

			summary_ws.write_array_formula('H4', '=IFERROR((B4/D4)-1,"")', comptotal_header)
			summary_ws.write_array_formula('H5', '=IFERROR((B5/D5)-1,"")', comptotal_header)
			summary_ws.write_array_formula('H6', '=IFERROR((B6/D6)-1,"")', comptotal_header)
			summary_ws.write_array_formula('H7', '=IFERROR((B7/D7)-1,"")', comptotal_header)

			# SpringServe & LKQD - Total
			summary_ws.write_array_formula('B11', '=IFERROR(B4+B18,"")', comp_header)
			summary_ws.write_array_formula('B12', '=IFERROR(B5+B19,"")', comp_header)
			summary_ws.write_array_formula('B13', '=IFERROR(B6+B20,"")', comp_header)
			summary_ws.write_array_formula('B14', '=IFERROR(B12/B11,"")', comptotal_header)

			summary_ws.write_array_formula('C11', '=IFERROR(C4+C18,"")', comp_header)
			summary_ws.write_array_formula('C12', '=IFERROR(C5+C19,"")', comp_header)
			summary_ws.write_array_formula('C13', '=IFERROR(C6+C20,"")', comp_header)
			summary_ws.write_array_formula('C14', '=IFERROR(C12/C11,"")', comptotal_header)

			summary_ws.write_array_formula('D11', '=IFERROR(D4+D18,"")', comp_header)
			summary_ws.write_array_formula('D12', '=IFERROR(D5+D19,"")', comp_header)
			summary_ws.write_array_formula('D13', '=IFERROR(D6+D20,"")', comp_header)
			summary_ws.write_array_formula('D14', '=IFERROR(D12/D11,"")', comptotal_header)

			# SpringServe & LKQD - % Change
			summary_ws.write_array_formula('G11', '=IFERROR((B11/C11)-1,"")', comptotal_header)
			summary_ws.write_array_formula('G12', '=IFERROR((B12/C12)-1,"")', comptotal_header)
			summary_ws.write_array_formula('G13', '=IFERROR((B13/C13)-1,"")', comptotal_header)
			summary_ws.write_array_formula('G14', '=IFERROR((B14/C14)-1,"")', comptotal_header)

			summary_ws.write_array_formula('H11', '=IFERROR((B11/D11)-1,"")', comptotal_header)
			summary_ws.write_array_formula('H12', '=IFERROR((B12/D12)-1,"")', comptotal_header)
			summary_ws.write_array_formula('H13', '=IFERROR((B13/D13)-1,"")', comptotal_header)
			summary_ws.write_array_formula('H14', '=IFERROR((B14/C14)-1,"")', comptotal_header)

			# LKQD - Total
			summary_ws.write_array_formula('B18', '=IF(Comparison!B57="","",Comparison!B57)', comp_header)
			summary_ws.write_array_formula('B19', '=IF(Comparison!C57="","",Comparison!C57)', comp_header)
			summary_ws.write_array_formula('B20', '=IF(Comparison!D57="","",Comparison!D57)', comp_header)
			summary_ws.write_array_formula('B21', '=IF(Comparison!E57="","",Comparison!E57)', comptotal_header)

			summary_ws.write_array_formula('C18', '=IF(Comparison!F57="","",Comparison!F57)', comp_header)
			summary_ws.write_array_formula('C19', '=IF(Comparison!G57="","",Comparison!G57)', comp_header)
			summary_ws.write_array_formula('C20', '=IF(Comparison!H57="","",Comparison!H57)', comp_header)
			summary_ws.write_array_formula('C21', '=IF(Comparison!I57="","",Comparison!I57)', comptotal_header)

			summary_ws.write_array_formula('D18', '=IF(Comparison!J57="","",Comparison!J57)', comp_header)
			summary_ws.write_array_formula('D19', '=IF(Comparison!K57="","",Comparison!K57)', comp_header)
			summary_ws.write_array_formula('D20', '=IF(Comparison!L57="","",Comparison!L57)', comp_header)
			summary_ws.write_array_formula('D21', '=IF(Comparison!M57="","",Comparison!M57)', comptotal_header)

			# LKQD - % Change
			summary_ws.write_array_formula('G18', '=IFERROR((B18/C18)-1,"")', comptotal_header)
			summary_ws.write_array_formula('G19', '=IFERROR((B19/C19)-1,"")', comptotal_header)
			summary_ws.write_array_formula('G20', '=IFERROR((B20/C20)-1,"")', comptotal_header)
			summary_ws.write_array_formula('G21', '=IFERROR((B21/C21)-1,"")', comptotal_header)

			summary_ws.write_array_formula('H18', '=IFERROR((B18/D18)-1,"")', comptotal_header)
			summary_ws.write_array_formula('H19', '=IFERROR((B19/D19)-1,"")', comptotal_header)
			summary_ws.write_array_formula('H20', '=IFERROR((B20/D20)-1,"")', comptotal_header)
			summary_ws.write_array_formula('H21', '=IFERROR((B21/D21)-1,"")', comptotal_header)




		today_sheet()
		yest_sheet()
		sdlw_sheet()
		comp_sheet()
		sum_sheet()
		wb.close()
