class SS:

	def post_req(dates):
		import springserve
		print("[+]Requesting SpringServe Data")
		d_range = ['today','yesterday','sdlw']
		data = dict()

		for i in d_range:
			report = springserve.reports.run(start_date=dates[i],
											 end_date=dates[i],
											 dimensions=[],
											 declared_domains=[],
											 interval="hour",
											 timezone="America/New_York",
											 metrics=["opportunities", "total_impressions", "revenue"]
			)
			df = report.to_dataframe()
			df = df.round({'revenue': 2})
			data[i] = df

		print(data['today'])
		return data