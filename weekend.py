import datetime
import sys, os

#sys.path.append(os.path.join(os.path.dirname(__file__),"./modules"))
from modules.ss import SS as ss
from modules.lkqd import LKQD as lq
from modules.excel import Excel as e


def get_dates():
	print("[+]Generating Dates")
	cur_date = datetime.datetime.today()

	sdlw = datetime.timedelta(weeks=1)
	sdlw = cur_date - sdlw
	sdlw = str(sdlw).split()[0]

	yest = datetime.timedelta(days=1)
	yest = cur_date - yest
	yest = str(yest).split()[0]

	cur_date = str(cur_date).split()[0]
	return {'today':cur_date,
			'yesterday':yest,
			'sdlw':sdlw
		   }

def main():
	dates = get_dates()
	ss_data = ss.post_req(dates)
	lkqd_data = lq.post_req(dates)
	e.build_doc(ss_data,lkqd_data,dates)



if __name__ == '__main__':
	main()